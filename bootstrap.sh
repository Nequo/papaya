# Intitial packages and config
sudo zypper dup
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
echo ".cfg" >> .gitignore
ssh-keygen -t ed25519 -C "<key>"
cat .ssh/id_ed25519.pub 
git clone --bare git@gitlab.com:Nequo/papaya.git $HOME/.cfg
config checkout
config config --local status.showUntrackedFiles no

# Neovim latest
sudo apt-get install ninja-build gettext libtool libtool-bin autoconf automake cmake g++ pkg-config unzip
./bootstrap.sh
(cd ~ ; mkdir git ; cd git ;  git clone https://github.com/neovim/neovim.git ; cd neovim ; make ;sudo make install)

# Neovim stable
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
mv nvim.appimage nvim
chmod +x nvim
sudo mv nvim /usr/local/bin/

# Neovim config
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
# Node for COC
curl -sL install-node.now.sh/lts | sudo bash

git clone git@gitlab.com:Nequo/mistborn.git
mkdir -p ~/.config/nvim/colors/
ln -s /home/nequo/git/mistborn/colors/mistborn.vim /home/nequo/.config/nvim/colors/mistborn.vim
mkdir -p ~/.config/nvim/autoload/lightline/colorscheme/
ln -s /home/nequo/git/mistborn/autoload/lightline/colorscheme/mistborn.vim /home/nequo/.config/nvim/autoload/lightline/colorscheme/mistborn.vim


# Audio fix for yoga-c930
sudo ln -s ~/misc/audio.conf /etc/modprobe.d/alsa.conf

# ZSH
sudo zypper install zsh
chsh -s $(which zsh)
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.config/zsh/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.config/zsh/zsh-syntax-highlighting

# Rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh\
curl -L https://github.com/rust-analyzer/rust-analyzer/releases/latest/download/rust-analyzer-linux -o rust-analyzer
chmod +x rust-analyzer
sudo mv rust-analyzer /usr/local/bin

# Gnome terminal
./misc/nord.sh

# Random stuff
sudo apt install gnome-tweak-tool

# Icons
git clone https://github.com/vinceliuice/Tela-icon-theme.git
cd Tela-icon-theme/
./install.sh orange

# Tmux
sudo apt install tmux
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
