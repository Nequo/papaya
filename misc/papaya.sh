#!/bin/bash

get_packages() {
    pcount=$(apt list --installed 2>/dev/null| wc -l)
    printf "   "
    printf " : %s\n" "$pcount"
}

get_release() {
    release=$(hostnamectl | grep "Operating System:" | cut -d ":" -f2 | xargs)
    printf "   "
    printf " : %s\n" "$release"
}

get_colors() {
    fg=$'\e[0m'

    printf "    : "
    for ((i=1; i<=8; i++)); do
        printf "%b%s%b" "\e[3${i}m" " " "$fg"
    done
    printf "\n"
}

printf "\n"
get_release
get_packages
get_colors
