HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory

autoload -Uz compinit
compinit

zstyle ':completion:*' menu select

source ~/.config/zsh/git-prompt.sh

IGLOO_ZSH_PROMPT_THEME_ALWAYS_SHOW_USER=true
IGLOO_ZSH_PROMPT_THEME_ALWAYS_SHOW_HOST=true
IGLOO_ZSH_PROMPT_THEME_HIDE_TIME=true

fpath=(~/.config/zsh $fpath)
autoload -U promptinit
promptinit
prompt igloo

EDITOR=nvim
alias vim=nvim
alias config='/usr/bin/git --git-dir=/home/nequo/.cfg/ --work-tree=/home/nequo'
compdef config=git
alias ls="ls --color"
# Enable Ctrl-x-e to edit command line
autoload -U edit-command-line
# Emacs style
zle -N edit-command-line
bindkey '^xe' edit-command-line
bindkey '^x^e' edit-command-line

# append
path+=('/home/nequo/go/bin')
# or prepend
#path=('/home/david/pear/bin' $path)
# export to sub-processes (make it inherited by child processes)
export PATH


#source "$HOME/.cargo/env"
source ~/.config/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/.config/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
