# Dotfiles

![Terminals](/img/papaya_screen.png)

These are my configuration files for Linux, currently using Solus.
They are being managed by the method described [here](https://www.atlassian.com/git/tutorials/dotfiles)

## Bootstrap

First thing to do is upgrade the system and reboot.

Then generate an ssh-key and associate it with gitlab:

```shell
ssh-keygen -t ed25519 -C "<key>"
cat .ssh/id_ed25519.pub 
```

Then pull in this repository:
```shell
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
echo ".cfg" >> .gitignore
git clone --bare git@gitlab.com:Nequo/papaya.git $HOME/.cfg
config checkout
config config --local status.showUntrackedFiles no
```

## Audio Fix

I use a lenovo-yoga c930 and the sound does not work on newer kernels so we need
to use the legacy driver:
```shell
sudo mkdir -p /etc/modprobe.d
sudo ln -s ~/misc/audio.conf /etc/modprobe.d/alsa.conf
```

## Neovim

I use neovim as a text editor with coc for lsp completion.
Install neovim:
```shell
(cd ~ ; mkdir git ; cd git ;  git clone https://github.com/neovim/neovim.git ; cd neovim ; make CMAKE_BUILD_TYPE=Release ;sudo make install)
```

Install node as a dependancy for CoC:
```shell
sudo eopkg install nodejs
```

Install vim-plug for managing plugins:
```shell
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
```

Install personal colorscheme and symlink for rapid changes:
```shell
mkdir git
git clone git@gitlab.com:Nequo/spacepirate.git git/spacepirate
mkdir -p ~/.config/nvim/colors/
ln -s /home/nequo/git/spacepirate/colors/spacepirate.vim /home/nequo/.config/nvim/colors/spacepirate.vim
```


## Zsh

Zsh provides some additional customisability compared to bash. I use the prompt
written by arcticicestudio in his [igloo](https://github.com/arcticicestudio/igloo/tree/master/snowblocks/zsh) repository
with some tweaks.

```shell
sudo eopkg install zsh
chsh -s $(which zsh)
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.config/zsh/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.config/zsh/zsh-syntax-highlighting
```

## Icon theme
I prefer the icons from the Tela theme:

```shell
git clone https://github.com/vinceliuice/Tela-icon-theme.git git/Tela-icon-theme
./git/Tela-icon-theme/install.sh orange
```

## Gestures

```shell
sudo eopkg it libinput-gestures
libinput-gestures-setup start
```
