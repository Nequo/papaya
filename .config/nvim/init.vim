let mapleader = "\<Space>"

call plug#begin()
  " Colorschemes
  Plug 'tjdevries/colorbuddy.vim'
  Plug 'arcticicestudio/nord-vim'
  Plug 'romainl/Apprentice'
  Plug 'https://gitlab.com/Nequo/mistborn.git'
  Plug 'mhartington/oceanic-next'

  "Utilities
  Plug 'tpope/vim-fugitive'
  Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
  Plug 'nvim-lua/popup.nvim'
  Plug 'nvim-lua/plenary.nvim'
  Plug 'nvim-telescope/telescope.nvim'

  " LSP
  Plug 'neovim/nvim-lspconfig'  
  Plug 'hrsh7th/nvim-compe'
  Plug 'kosayoda/nvim-lightbulb'

  " Language-specific
  Plug 'hashivim/vim-terraform'
  Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

  " UI
  Plug 'itchyny/lightline.vim'
  Plug 'norcalli/nvim-colorizer.lua'
  Plug 'ryanoasis/vim-devicons'
call plug#end()


set termguicolors
let g:sonokai_style = 'maia'
colorscheme OceanicNext
let g:lightline = {
  \ 'colorscheme': 'oceanicnext',
  \ 'component_function': {
  \   'filetype': 'MyFiletype',
  \ }
  \ }

function! MyFiletype()
  return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype . ' ' . WebDevIconsGetFileTypeSymbol() : '') : ''
endfunction

" Telescope mappings
nnoremap <leader>ff <cmd>lua require('telescope.builtin').find_files()<cr>
nnoremap <leader>fg <cmd>lua require('telescope.builtin').live_grep()<cr>
nnoremap <leader>fb <cmd>lua require('telescope.builtin').buffers()<cr>
nnoremap <leader>fh <cmd>lua require('telescope.builtin').help_tags()<cr>

nnoremap <leader>ga <cmd>lua require('telescope.builtin').lsp_code_actions()<cr>

" Set completeopt to have a better completion experience
" :help completeopt
" menuone: popup even when there's only one match
" noinsert: Do not insert text until a selection is made
" noselect: Do not select, force user to select one from the menu
set completeopt=menuone,noinsert,noselect

" Avoid showing extra messages when using completion
set shortmess+=c


" Vim-go shortcuts
autocmd FileType go nmap <leader>b  <Plug>(go-build)
autocmd FileType go nmap <leader>r  <Plug>(go-run)
autocmd FileType go nmap <leader>t  <Plug>(go-test)
let g:go_echo_go_info = 0


" Common settings
set tabstop=4                       " number of visual spaces per TAB
set softtabstop=4                   " number of spaces in tab when editing
set expandtab                       " tabs are spaces
set autoindent                      " always turn on indentation
set breakindent                     " Wrap lines at same indent level
set shiftwidth=4                    " Spaces to use for autoindenting
set backspace=indent,eol,start      " proper backspace behavior


set number
set showcmd                         " show command in bottom bar
set noshowmode
set cursorline                      " highlight current line
filetype indent on                  " load filetype-specific indent files
set wildmenu                        " visual autocomplete for command menu
set wildmode=longest,full           " Enable file autocomplete in command mode
set lazyredraw                      " redraw only when we need to.
set showmatch                       " highlight matching [{()}]
set scrolloff=15                    " always leave 15 spaces when scrolling
set linebreak                       " don't wrap words
set timeoutlen=300 ttimeoutlen=10   " Eliminate delay when changing mode
set splitbelow                      " horizontal split opens below
set splitright                      " Vertical split opens to the right
set incsearch                       " search as characters are entered
set hlsearch                        " highlight matches
set signcolumn=yes                  " Always show the sign column


set updatetime=200

map gm :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

lua require 'lsp'
lua require 'misc'
