" =============================================================================
" Filename: autoload/lightline/colorscheme/spacecamp.vim
" Author: Nadim Gomez
" License: MIT License
" =============================================================================
let s:fg = '#DEDEDE'
let s:bg = '#121212'
let s:grey = '#262626'
let s:lightgrey = '#969896'
let s:base2 = '#bababa'
let s:base01 = '#555555'

let s:red = '#821A1A'
let s:orange = '#F66100'
let s:yellow = '#f2ce00'
let s:green = '#81c13b'
let s:cyan = '#35b9ab'
let s:blue = '#91AADF'
let s:magenta = '#CF73E6'

let s:p = {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}
let s:p.normal.left = [ [ s:bg, s:blue ], [ s:fg, s:grey ] ]
let s:p.normal.right = [ [ s:bg, s:blue ], [ s:fg, s:grey ] ]
let s:p.inactive.right = [ [ s:fg, s:grey ], [ s:fg, s:grey ] ]
let s:p.inactive.left =  [ [ s:fg, s:grey ], [ s:fg, s:grey ] ]

let s:p.normal.middle = [ [ s:fg, s:grey ] ]
let s:p.inactive.middle = [ [ s:fg, s:grey ] ]

let s:p.insert.left = [ [ s:bg, s:green ], [ s:fg, s:grey ] ]
let s:p.insert.right = [ [ s:bg, s:green ], [ s:fg, s:grey ] ]
let s:p.replace.left = [ [ s:bg, s:orange ], [ s:fg, s:grey ] ]
let s:p.replace.right = [ [ s:bg, s:orange ], [ s:fg, s:grey ] ]
let s:p.visual.left = [ [ s:bg, s:magenta ], [ s:fg, s:grey ] ]
let s:p.visual.right = [ [ s:bg, s:magenta ], [ s:fg, s:grey ] ]


let s:p.tabline.left = [ [ s:base2, s:base01 ] ]
let s:p.tabline.tabsel = [ [ s:base2, s:base01 ] ]
let s:p.tabline.middle = [ [ s:base01, s:bg ] ]
let s:p.tabline.right = copy(s:p.normal.right)
let s:p.normal.error = [ [ s:red, s:grey ] ]
let s:p.normal.warning = [ [ s:yellow, s:grey ] ]

let g:lightline#colorscheme#spacecamp#palette = lightline#colorscheme#fill(s:p)

